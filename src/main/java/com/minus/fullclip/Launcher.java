package com.minus.fullclip;


import com.minus.fullclip.server.Server;

/**
 * Created by minus on 5/5/16.
 */
public class Launcher {
    public static void main(String[] args) {
        new Server().start();
    }
}
