package com.minus.fullclip.server;

/**
 * @author asafb
 */

import com.minus.fullclip.client.ClientProcessesManager;
import com.minus.fullclip.utils.OSUtils;
import com.minus.fullclip.utils.ServerUtils;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class Server extends Thread {
    final static private Logger logger = Logger.getLogger(Server.class.getName());

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(ServerUtils.findRandomPort())) {
            OSUtils.notification("Server running on " + serverSocket.getLocalPort() + ".");

            while (true) {
                executorService.submit(new ClientProcessesManager(serverSocket.accept())); //Got a new client in here
                OSUtils.notification("Detected new client");
            }

        } catch (IOException ex) {
            OSUtils.notification("Port appears to be closed(" + ex.getMessage() + "), try find another.");
        }

    }
}
