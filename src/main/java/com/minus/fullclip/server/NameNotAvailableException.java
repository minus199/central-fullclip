package com.minus.fullclip.server;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class NameNotAvailableException extends Exception {

    public NameNotAvailableException(String name) {
        super("Name " + name + " appears to be taken. Be original.");
    }

}
