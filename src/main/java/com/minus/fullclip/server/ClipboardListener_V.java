package com.minus.fullclip.server;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class ClipboardListener_V implements Runnable, ClipboardOwner {

    public static void main(String[] args) {
        new Thread(new ClipboardListener_V()).start();
    }

    private final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

    @Override
    public void run() {
        Transferable trans = clipboard.getContents(this);
        regainOwnership(trans);
        System.out.println("Listening to board...");
        while (true) {
        }
    }

    @Override
    public void lostOwnership(Clipboard c, Transferable t) {
        System.out.println(getClipboardContents());
//        Transferable contents = clipboard.getContents(this); //EXCEPTION
//        processContents(contents);
        regainOwnership(t);
    }

    void regainOwnership(Transferable t) {
        clipboard.setContents(t, this);
    }

    /**
     * Get the String residing on the clipboard.
     *
     * @return any text found on the Clipboard; if none found, return an empty
     * String.
     */
    public String getClipboardContents() {
        try {
            String result = "";
            //odd: the Object param of getContents is not currently used
            Transferable contents = clipboard.getContents(null);

            if (contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                //result = String.valueOf(contents.getTransferData(DataFlavor.stringFlavor));
                Object o = contents.getTransferData(DataFlavor.stringFlavor);
                String data = (String) contents.getTransferData(DataFlavor.stringFlavor);
                System.out.println("Clipboard contents: " + data);
            }

            return result;
        } catch (UnsupportedFlavorException | IOException ex) {
            Logger.getLogger(ClipboardListener_V.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "";
    }

    /**
     * Place a String on the clipboard, and make this class the owner of the
     * Clipboard's contents.
     */
    public void setClipboardContents(String aString) {
        StringSelection stringSelection = new StringSelection(aString);
        clipboard.setContents(stringSelection, this);
    }

}
