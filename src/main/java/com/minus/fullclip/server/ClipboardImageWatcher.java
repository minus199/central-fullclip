package com.minus.fullclip.server;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
 
import javax.imageio.ImageIO;
 
/**
 * @author Vincent Zurczak
 */
public class ClipboardImageWatcher implements ClipboardOwner {
     
    static final File PREVIEW_DIRECTORY = new File( System.getProperty( "java.io.tmpdir" ), "Image-Store" );
    private final List<PreviewFileListener> listeners;
    private final SimpleDateFormat sdf;
    private final AtomicBoolean run;
     
     
    /**
     * Constructor.
     */
    public ClipboardImageWatcher() {
         this.listeners = new ArrayList<PreviewFileListener> ();
         this.sdf = new SimpleDateFormat( "yy-MM-dd---HH-mm-ss" );
         this.run = new AtomicBoolean( true );
          
         if( ! PREVIEW_DIRECTORY.exists()
                 && ! PREVIEW_DIRECTORY.mkdir()) {
             // TODO: process this case in a better way
             System.out.println( "The preview directory could not be created." );
         }
    }
     
     
    /**
     * @param e
     * @return
     * @see java.util.List#add(java.lang.Object)
     */
    public boolean addPreviewFileListener( PreviewFileListener e ) {
        synchronized( this.listeners ) {
            return this.listeners.add( e );
        }
    }
 
 
    /**
     * @param o
     * @return
     * @see java.util.List#remove(java.lang.Object)
     */
    public boolean removePreviewFileListener( PreviewFileListener o ) {
        synchronized( this.listeners ) {
            return this.listeners.remove( o );
        }
    }
     
     
    /**
     * @see java.awt.datatransfer.ClipboardOwner
     * #lostOwnership(java.awt.datatransfer.Clipboard, java.awt.datatransfer.Transferable)
     */
    @Override
    public void lostOwnership( Clipboard clipboard, Transferable contents ) {
        // Acquire the ownership again is not working very well for a periodic check
    }
     
     
    /**
     * Starts polling the clipboard.
     */
    public void start() {
         
        // Define the polling delay
        final int pollDelay = 5000;
         
        // Create the runnable
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                 
                Transferable contents = null;
                while( ClipboardImageWatcher.this.run.get()) {
                     
                    // Get the clipboard's content
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    try {
                        contents = clipboard.getContents( null );
                        if ( contents == null
                                || ! contents.isDataFlavorSupported( DataFlavor.imageFlavor ))
                            continue;
                         
                        Image img = (Image) clipboard.getData( DataFlavor.imageFlavor );
                        saveClipboardImage( img );
                        clipboard.setContents(new StringSelection( "The clipboard watcher was here!" ), ClipboardImageWatcher.this );
                         
                    } catch( UnsupportedFlavorException ex ) {
                        ex.printStackTrace();
 
                    } catch( IOException ex ) {
                        ex.printStackTrace();
                         
                    } catch( Exception e1 ) {
                        // We get here if we could not get the clipboard's content
                        continue;
                     
                    } finally {                     
                     
                        try {
                            Thread.sleep( pollDelay );
                             
                        } catch( InterruptedException e ) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
         
        // Run it in a thread
        Thread thread = new Thread( runnable );
        thread.start();
    }
     
     
    /**
     * Stops listening.
     */
    public void stop() {
        this.run.set( false );
    }
     
 
    /**
     * Saves the image residing on the clip board.
     */
    public void saveClipboardImage( Image img ) {
 
        if( img != null ) {         
            if( img instanceof BufferedImage ) {
                try {
                    File file = new File( PREVIEW_DIRECTORY, this.sdf.format( new Date()) + ".jpg" );
                    ImageIO.write((BufferedImage) img, "jpg", file );
                    for( PreviewFileListener listener : this.listeners )
                        listener.newCreatedFile( file );
 
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
 
            } else {
                System.out.println( "Unsupported image: " + img.getClass());
            }   
        }
    }
     
     
    /**
     * A interface to listen to image detection.
     */
    public interface PreviewFileListener {
 
        /**
         * Notifies this instance that an image file was created from the clip board.
         * @param imageFile the image file that was saved on the disk
         */
        void newCreatedFile( File imageFile );
    }
}