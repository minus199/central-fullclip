package com.minus.fullclip.utils;

import java.io.IOException;

/**
 * Created by minus on 5/4/16.
 */
public class OSUtils {

    /**
     * Default waiting 1 sec
     * @param text
     * @throws IOException
     */
    public static void notification(String text){
        notification(text, 1000L);
    }

    public static void notification(String text, Long millis){
        String[] cmd = {"/usr/bin/notify-send",
                "-t",
                millis.toString(),
                text
        };

        try {
            Runtime.getRuntime().exec(cmd);
        } catch (IOException ignored) {

        }
    }
}
