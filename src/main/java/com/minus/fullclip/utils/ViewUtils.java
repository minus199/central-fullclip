package com.minus.fullclip.utils;

import java.awt.Color;
import java.util.Random;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class ViewUtils {

    public static Color randomColor() {
        Random random = new Random();
        
        float r = random.nextFloat();
        float g = random.nextFloat();
        float b = random.nextFloat();

        
        return new Color(r, g, b);
        
    }
}
