package com.minus.fullclip.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.stream.Collectors;

/**
 * Created by minus on 4/29/16.
 */
public class ClientUtils {
    public static String extractMacAddress() throws SocketException {
        String[] max = null;

        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();

        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface current = networkInterfaces.nextElement();

            Enumeration<InetAddress> inetAddresses = current.getInetAddresses();
            while (inetAddresses.hasMoreElements()) {
                InetAddress inetAddress = inetAddresses.nextElement();
                byte[] mac = inetAddress.getAddress();

                String currentMac = "";
                for (int i = 0; i < mac.length; i++) {
                    currentMac = currentMac + String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : "");
                }
                String[] currentMacSplit = currentMac.split("");
                if (max == null) {
                    max = currentMacSplit;
                    continue;
                }

                //the value 0 if x == y; a value less than 0 if x < y; and a value greater than 0 if x > y
                final int localMax = Integer.valueOf(max.length).compareTo(currentMacSplit.length);
                if (localMax == 0) {
                    for (int i = 0; i < max.length; i++) {
                        final int i1 = max[i].compareTo(currentMacSplit[i]);
                        if (i1 != 0) {
                            max = i1 > 0 ? max : currentMacSplit;
                            break;
                        }
                    }
                }

            }
        }


        return (max != null) ? Arrays.asList(max).stream().collect(Collectors.joining("")) : "";
    }
}
