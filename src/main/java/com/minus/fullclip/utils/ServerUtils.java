package com.minus.fullclip.utils;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class ServerUtils {

    public static int findRandomPort() {
        int min = 49152;
        int max = 65535;
        int range = max - min;

        int a = min + (int) (range * Math.random());
        if (a < min || a > max) {
           return findRandomPort();
        }

        return 12345; //return a;
    }



}
