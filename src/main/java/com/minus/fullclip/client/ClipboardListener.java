package com.minus.fullclip.client;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class ClipboardListener implements ClipboardOwner,Runnable {

    private final Clipboard sysClip;
    private final ArrayBlockingQueue<String> stringsQueue;

    ClipboardListener() {
        this.stringsQueue = ClipboardQueue.getInstance();
        sysClip = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    public void run() {
        regainOwnership(sysClip.getContents(this));
        System.out.println("Listening to board...");

        while (true) {
        }
    }

    public void lostOwnership(Clipboard c, Transferable t) {
        Transferable contents = sysClip.getContents(this); //EXCEPTION
        processContents(contents);
        regainOwnership(contents);
    }

    void processContents(Transferable t) {
        try {

//            InputStreamReader inputStreamReader = (InputStreamReader) t.getTransferData(DataFlavor.stringFlavor);
//            final String s = IOUtils.toString(inputStreamReader);

            String s = (String) t.getTransferData(DataFlavor.stringFlavor);
            stringsQueue.put(s);
        } catch (Exception ex) {
            Logger.getLogger(ClipboardListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void regainOwnership(Transferable t) {
        sysClip.setContents(t, this);
    }


}


