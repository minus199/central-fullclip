package com.minus.fullclip.client;


import com.minus.fullclip.utils.OSUtils;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by minus on 5/5/16.
 *
 * Listens on queue for new clipboard data and submits to the specific client
 */
class QueueListener implements Runnable{
    final private ObjectOutputStream outToClient;

    QueueListener(ObjectOutputStream outToClient) {
        this.outToClient = outToClient;
    }

    @Override
    public void run() {
        while (true){
            try {
                final String take = ClipboardQueue.getInstance().take();
                outToClient.writeObject(take);
            } catch (InterruptedException | IOException e) {
                OSUtils.notification("Connection seems to be reset: " + e.getMessage());
                return;
            }
        }
    }
}
