package com.minus.fullclip.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by minus on 5/5/16.
 */
public class ClientProcessesManager implements Runnable{
    final private ClientHandler clientHandler;
    final private ClipboardListener clipboardListener;
    final private QueueListener queueListener;

    final private ObjectInputStream inStream;
    final private ObjectOutputStream outStream;


    private Boolean stopped = false;

    private final ExecutorService executorService = Executors.newFixedThreadPool(3);


    public ClientProcessesManager(Socket socket) {

        try {
            inStream = new ObjectInputStream(socket.getInputStream());
            outStream = new ObjectOutputStream(socket.getOutputStream());

            this.clientHandler = new ClientHandler(inStream);
            executorService.submit(clientHandler);
        } catch (IOException e) {
            throw new RuntimeException("Unable to init socket", e);
        }

        this.clipboardListener = new ClipboardListener();
        executorService.submit(clipboardListener);

        this.queueListener = new QueueListener(outStream);
        executorService.submit(queueListener);

    }

    public ClientHandler getClientHandler() {
        return clientHandler;
    }

    public ClipboardListener getClipboardListener() {
        return clipboardListener;
    }

    public QueueListener getQueueListener() {
        return queueListener;
    }

    @Override
    public void run() {
        while (!stopped){
            // keeping the spirit alive......
        }
    }

    final public void close(){
        try {
            inStream.close();
        } catch (IOException e) {
        }

        try {
            outStream.close();
        } catch (IOException e) {

        }

        executorService.shutdownNow();

        stopped = true;
    }
}
