package com.minus.fullclip.client;

import com.minus.fullclip.utils.OSUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.*;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class ClientHandler implements Runnable {

    final private ObjectInputStream inFromClient;

    ClientHandler(ObjectInputStream inFromClient) throws IOException {
        this.inFromClient = inFromClient;
    }

    @Override
    public void run() {
        while (true) {
            Object input = null;
            try {
                input = waitForClipboardData();
            } catch (IOException | ClassNotFoundException e) {
                OSUtils.notification(e.getMessage());
            }

            setClipboardContent(input);
        }
    }

    private Object waitForClipboardData() throws IOException, ClassNotFoundException {
        Object input = inFromClient.readObject();

        while (input == null) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return input;
    }

    private void setClipboardContent(Object input) {
        StringSelection selection = new StringSelection(input + "--");
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        OSUtils.notification("Got " + input, 200L);
    }
}
